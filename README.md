# XenServer Backup

The `xenbackup` script automates snapshot backups of XenServer VMs creating
templates that can be use to quickly recreate a new VM. Designating the VMs,
how often to backup them up, and how many backups to keep is done via custom
fields on the VM objects themselves typically in XenCenter. Defaults are sane
and safe so it's hopefully simple to deploy.

# Installation 
Use something like `wget` to copy the script into `/root/bin` or someplace else
on the XenServer master machine from [here](xenbackup) and `chmod 755` to make
it executable. Then create `/etc/cron.daily/xenbackup` with the content below.

    #!/bin/sh
    /root/bin/xenbackup -c

Use the path where you coppied the script. Adjust the command-line options as
described in [Customization](#customization) below.

Then, fire up XenCenter and edit the properties of a VM to be backed up.
Under "Custom Fields", add `xenbackup` and `xenbackup-keep` fields. Set
`xenbackup` to `daily`, `weekly`, or `monthly`. Leave `xenbackup-keep` empty
and the script will keep the current and one previous backup. Set it to a
number higher than `1` to keep more.

# Customization
Command-line options can be used to customize the default behaviour of the
script.

## Logging
The script will log high-level progress (INFO) along with warnings and errors to
STDERR by default. Use the `-v` option to increase the level of detail logged to
NOTE. Use it again to add DEBUG logging. The `-q` option resets the detail down
to ERRORs only.

The `-c` option causes the script to use the `logger` utility to send
diagnostic output to syslog instead of STDERR. Typically, this is used when
running under cron.

## Storage Repository
`xenbackup` will create backup templates for VMs on the same storage repository
as the VM's first VDI by default. Use the `-s UUID` command-line option to
specify the UUID of a different SR where the templates should be stored.

## Field Names
By default, `xenbackup` looks for running VMs with custom `xenbackup` fields and
uses that value to decide whether and when to it back it up. If the field does
not exist or is empty (or is set to an invalid value) then the VM isn't touched.
Use the `-f FIELD` command-line option to use another field name.

Append `-keep` to that field name to come up with the name of another field that
is used to specify the number of backups to keep; i.e. `xenbackup-keep`.

## Default Keep Count
The number of backups to keep defaults to 1 but can be overriden globally with
the `-k KEEP` command-line option.  Override it for individual VMs using the
`xenbackup-keep` custom field as discussed above.

## Snapshot & Template Names
By default, temporary VM snapshots are named `xenbackup`. Use the `-n NAME`
commant-line option to override this. The same value is used when naming the
resulting templates; i.e. "VM-NAME-DATE".

## Weekly & Monthly Day of Week
When the `xenbackup` field for a VM is set to `weekly` or `monthly`, the script
will create these backups on Sundays by default. Use the `-d DAY` command-line
option to change to another day. The value for the option should be the short
3-character abbreviation for the day name; i.e. `mon`, `tue`, `wed`, etc.

## VMs to Backup
The script will scan running VMs for any with the `xenbackup` field set as 
described [earlier](#field-names). To instead backup VMs that aren't running,
specify their names on the command line; i.e. `xenbackup -v VM1 VM2`. Whether
to backup the VM is still controlled by the value of the `xenbackup`.

# Support
`xenbackup` is developed and maintained by Paul Dugas <paul@dugasent.com>
originally for internal use and later for various clients. The project is hosted
at <https://git.dugasent.com/pdugas/xenbackup/>. Submit feature
requests or defects as [issues] there please.

# License

Copyright (C) 2017-2018 Paul Dugas.  All rights reserved.

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. See [LICENSE](LICENSE) or <http://www.gnu.org/licenses/>.

[issues]: https://git.dugasent.com/pdugas/xenbackup/issues/

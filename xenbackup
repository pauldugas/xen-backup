#!/bin/sh
# -----------------------------------------------------------------------------
# XenServer Backup
# Copyright (C) 2018 Paul Dugas
# 
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
# more details.
# 
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <http://www.gnu.org/licenses/>.
#
# The project is hosted at http://git.dugasent.com/pdugas/xenbackup/.
# -----------------------------------------------------------------------------

# Constants
VERSION="v1.0"
DOW=$(date +'%a' | tr '[:upper:]' '[:lower:]') # today's day-of-week; i.e mon
DOM=$(date +'%e')                              # today's day-of-month; i.e 6
DATE=$(date +"%Y%m%d")                         # suffix for backups, YYYYMMDD
ERROR=0; WARN=1; INFO=2; NOTE=3; DEBUG=4       # log levels

# Runtime Settings
VERBOSE=$INFO      # Default log level.
CRON=              # Non-empty for "cron" mode; logs to syslog.
DAY="sun"          # Day of week for weekly and monthly backups
SR=                # SR UUID
FIELD="xenbackup"  # Name of custom field in VM properties
SUFFIX=$FIELD      # snapshot name and template name component
DEFAULT_KEEP=1     # number to keep if not specified

# Logging
function _log()
{
  LEVEL=$1; shift
  TAG=$1; shift
  PRIORITY=$1; shift
  if [ $LEVEL -le $VERBOSE ]; then
    if [ "$CRON" = "" ]; then
      echo "$(date +"%b %d %H:%M:%S") [$TAG] $*" 1>&2
    else
      logger -t xenbackup -p user.$PRIORITY "$*"
    fi
  fi
}
function error() { _log $ERROR ERROR error   $*; }
function warn()  { _log $WARN  WARN  warning $*; }
function info()  { _log $INFO  INFO  info    $*; }
function note()  { _log $NOTE  NOTE  notice  $*; }
function debug() { _log $DEBUG DEBUG debug   $*; }
function fatal() { _log $ERROR FATAL crit    $*; exit 1; }

# Utilities
function version()
{
  cat << EOF
XenServer Backup $VERSION
Copyright (C) 2017-$(date +"%Y") Paul Dugas

This program comes with ABSOLUTELY NO WARRANTY. This is free software, and you
are welcome to redistribute it under certain conditions; see the LICENSE file
in the distribution for details.
EOF
  exit 0
}

function usage()
{
  cat << EOF
Usage: xenbackup [-h] [-v|-q] -s UUID [-n NAME] [-f FIELD] [-k KEEP] [-d DAY]

Options:
    -h         Display usage and exit.
    -V         DIsplay version ingo and exit.
    -v         Increase logging detail. Multiple for more.
    -q         Reduce logging to errors only.
    -s UUID    Storage repository UUID. (required)
    -n NAME   Snapshot name and template name suffic (default=$SUFFIX)
    -f FIELD   Custom field name to use (default=$FIELD)
    -k KEEP    Default number of backup to keep (default=$DEFAULT_KEEP)
    -d DAY     Set day-of-week for weekly backups (default=$DAY)

EOF
  exit 0
}

function xe_param()
{
  PARAM=$1
  while read DATA; do
    LINE=$(echo $DATA | egrep "$PARAM")
    if [ $? -eq 0 ]; then echo "$LINE" | awk 'BEGIN{FS=": "}{print $2}'; fi
  done
}

function xe_run()
{
  NAME=$1; shift

  debug "$NAME: Running \"xe $*\"."
  OUT=$(xe $* 2>&1)
  RET=$?

  if [ "$RET" != "0" ]; then
    error "$NAME: $OUT"
    fatal "$NAME: $1 failed."
  fi

  debug "$NAME: $1 succeeded"
  echo "$OUT"
}

function xe_delete_snapshot()
{
  NAME=$1; UUID=$2

  for VDI in $(xe_run $NAME vbd-list vm-uuid=$UUID empty=false | xe_param vdi-uuid); do
    debug "$NAME: Deleting VDI $VDI"
    xe_run $NAME vdi-destroy uuid=$VDI >/dev/null
  done

  debug "$NAME: Deleting snapshot $UUID"
  xe_run $NAME snapshot-uninstall uuid=$UUID force=true >/dev/null
}

function xe_delete_template()
{
  NAME=$1; UUID=$2

  for VDI in $(xe_run $NAME vbd-list vm-uuid=$UUID empty=false | xe_param vdi-uuid); do
    debug "$NAME: Deleting VDI $VDI"
    xe_run $NAME vdi-destroy uuid=$VDI >/dev/null
  done

  debug "$NAME: Deleting template $UUID"
  xe_run $NAME template-uninstall template-uuid=$UUID force=true >/dev/null
}

# Options
while getopts ":hVvqcs:n:f:d:" OPT; do
  case ${OPT} in
    h )
      usage
      ;;
    V )
      version
      ;;
    v )
      VERBOSE=$(expr $VERBOSE + 1)
      ;;
    q )
      VERBOSE=0
      ;;
    c )
      CRON=1
      ;;
    s )
      SR=$(echo $OPTARG)
      ;;
    n )
      SUFFIX=$OPTARG
      ;;
    f )
      FIELD=$OPTARG
      ;;
    d )
      DAY=$(echo $OPTARG | tr '[:upper:]' '[:lower:]')
      ;;
    \? )
      echo "Invalid option: -$OPTARG" 1>&2
      exit 1
      ;;
  esac
done
shift $((OPTIND -1))

# Make sure we have the XE command
if ! hash xe 2>/dev/null ; then
  fatal "Missing 'xe' command!"
fi

# Start
exec 200< $0
if ! flock -n 200; then
    fatal "Failed to get lock!"
fi

info ">> XenBackup Started"

# Check SR UUID
if [ "$SR" != "" ]; then
  SR_NAME=$(xe_run HOST sr-list uuid=$SR | xe_param name-label)
  if [ "$SR_NAME" = "" ]; then
    fatal "Invalid SR UUID!"
  fi
fi

# VMs to Backup
if [ "$*" != "" ]; then
  for ARG in $*; do
    debug "Looking up UUID for \"$ARG\"."
    UUID=$(xe_run HOST vm-list name-label=$ARG | xe_param uuid)
    if [ "$UUID" = "" ]; then
      fatal "Didn't find VM named \"$ARG\"!"
    else
      VMS="$VMS $UUID"
    fi
  done
else
  VMS=$(xe_run HOST vm-list power-state=running is-control-domain=false | xe_param uuid)
fi

# For each VM...
for VM in $VMS; do

  NAME="$(xe_run HOST vm-list uuid=$VM | xe_param name-label)"
  debug "$NAME: uuid=$VM"

  FREQ=$(xe_run $NAME vm-param-get uuid=$VM param-name=other-config \
         param-key=XenCenter.CustomFields.$FIELD 2>/dev/null \
         | tr '[:upper:]' '[:lower:]')
  if [ "$FREQ" = "" ]; then
    note "$NAME: Skipped. \"$FIELD\" field not set."
    continue
  fi
  debug "$NAME: \"$FIELD\" field is \"$FREQ\"."

  KEEP=$(xe_run $NAME vm-param-get uuid=$VM param-name=other-config \
         param-key=XenCenter.CustomFields.$FIELD-keep 2>/dev/null \
         | tr '[:upper:]' '[:lower:]')
  if [ "$KEEP" = "" ]; then
    debug "$NAME: Using default \"$FIELD-keep\" setting of $DEFAULT_KEEP."
    KEEP="$DEFAULT_KEEP"
  else
    if ! [[ $KEEP =~ ^[0-9]+$ ]]; then
      error "$NAME: Skipped. Invalid \"$FIELD-keep\" value \"$KEEP\"."
      continue
    fi
    debug "$NAME: \"$FIELD-keep\" field is \"$KEEP\"."
  fi

  if [ "$FREQ" = "weekly" -o "$FREQ" = "week" ]; then
    if [ "$DOW" != "$DAY" ]; then
      note "$NAME: Skipped. Today's not \"$DAY\"."
      continue
    fi
    note "$NAME: Running weekly backup."
  elif [ "$FREQ" = "monthly" -o "$FREQ" = "month" ]; then
    if [ "$DOW" != "$DAY" ]; then
      note "$NAME: Skipped. Today's not \"$DAY\"."
      continue
    fi
    if [ $DOM -gt 7 ]; then
      note "$NAME: Skipped. Today's not the first week of the month."
      continue
    fi
    note "$NAME: Running monthly backup."
  elif [ "$FREQ" = "daily" -o "$FREQ" = "day" ]; then 
    note "$NAME: Running daily backup."
  else 
    error "$NAME: Skipped. Invalid \"backup\" value; \"$FREQ\"."
    continue
  fi

  VM_SR=$SR
  if [ "$VM_SR" = "" ]; then
    debug "$NAME: Discovering SR."
    VDI=$(xe_run $NAME vbd-list vm-uuid=$VM empty=false | xe_param vdi-uuid | head -1)
    debug "$NAME: VDI is $VDI."
    VM_SR=$(xe_run $NAME vdi-list uuid=$VDI | xe_param sr-uuid)
    VM_SR_NAME=$(xe_run $NAME sr-list uuid=$VM_SR | xe_param name-label)
    debug "$NAME: SR is \"$VM_SR_NAME\", $VM_SR."
  fi
  [ "$VM_SR" != "" ] || fatal "$NAME: Failed to find SR!"

  SNAP_UUID=$(xe_run $NAME snapshot-list name-label=$SUFFIX | xe_param uuid)
  if [ "$SNAP_UUID" != "" ]; then
    note "$NAME: Deleting old \"$SUFFIX\" snapshot."
    xe_delete_snapshot $NAME $SNAP_UUID
  fi

  note "$NAME: Creating snapshot \"$SUFFIX\"."
  SNAP_UUID=$(xe_run $NAME vm-snapshot uuid=$VM new-name-label="$SUFFIX")
  debug "$NAME: New snapshot UUID is $SNAP_UUID."

  TEMP="$(xe_run $NAME template-list name-label="$NAME-$SUFFIX" | xe_param uuid)"
  if [ "$TEMP" != "" ]; then
    note "$NAME: Deleting leftover \"$NAME-$SUFFIX\" template."
    xe_delete_template $NAME $TEMP
  fi

  note "$NAME: Copying snapshot to template; \"$NAME-$SUFFIX\"."
  TMPL_UUID=$(xe_run $NAME snapshot-copy uuid=$SNAP_UUID sr-uuid=$VM_SR \
         new-name-description=\"Backup created by xenbackup $(date)\" \
         new-name-label="$NAME-$SUFFIX")
  debug "$NAME: New template UUID is $TMPL_UUID."

  note "$NAME: Deleting \"$SUFFIX\" snapshot."
  xe_delete_snapshot $NAME $SNAP_UUID

  for OLD in $(xe_run $NAME template-list is-default-template=false | egrep "name-label .*: *$NAME-$SUFFIX-$DATE" | xe_param name-label) ; do
    OLD_UID=$(xe_run $NAME template-list name-label="$OLD" | xe_param uuid)
    debug "$NAME: Removing earlier backup from today; \"$OLD\"."
    xe_delete_template $NAME $OLD_UID
  done

  for OLD in $(xe_run $NAME template-list is-default-template=false | egrep "name-label .*: *$NAME-$SUFFIX-[0-9]*" | xe_param name-label | sort -n | head -n-$KEEP) ; do
    OLD_UID=$(xe_run $NAME template-list name-label="$OLD" | xe_param uuid)
    debug "$NAME: Removing outdated backup; \"$OLD\"."
    xe_delete_template $NAME $OLD_UID
  done

  note "$NAME: Created \"$NAME-$SUFFIX-$DATE\" template."
  xe_run $NAME template-param-set name-label="$NAME-$SUFFIX-$DATE" uuid=$TMPL_UUID

done

info "<< XenBackup Complete"

# -----------------------------------------------------------------------------
# vim: set sw=2 ts=2 et :
